#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
import re
from urllib.request import urlopen

from bs4 import BeautifulSoup
import PyRSS2Gen


BASE_URL = 'https://www.information.dk'

page = 0
rss_items = []

while len(rss_items) < 25:
    html = urlopen('{}/nyheder?page={}'.format(BASE_URL, page)).read()
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find('ul', class_='ruled-grey').find_all('li')

    for item in items:
        link = item.find_all('a')[1]['href']
        if not link.startswith('/telegram/'):
            continue
        link = '{}{}'.format(
            BASE_URL,
            link
        )
        title = item.find_all('h3')[0].string.strip()
        date = datetime.strptime(
            item.find_all('span', property='dc:date')[0]['content'][:19],
            '%Y-%m-%dT%H:%M:%S'
        )
        item_html = urlopen(link).read()
        item_soup = BeautifulSoup(item_html, 'html.parser')
        description = item_soup.find(
            class_='field field-name-field-ritzau-subheader'
        )
        description = '' if description is None else description.string
        rss_items.append(PyRSS2Gen.RSSItem(
            title=title,
            description=description,
            link=link,
            guid = PyRSS2Gen.Guid(link),
            pubDate=date,
        ))

        if len(rss_items) >= 25:
            break

    page += 1

feed = PyRSS2Gen.RSS2(
    title='Telegrammer fra Information.dk',
    link='https://www.information.dk/nyheder',
    description='Feed fra "Seneste nyheder"-siden på information.dk',
    lastBuildDate=datetime.now(),
    items=rss_items
)

print(feed.to_xml(encoding='utf-8'))
